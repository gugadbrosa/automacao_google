class SearchPage
  include Capybara::DSL

  def open_search(site)
    return visit site
  end

  def search_field(input_name)
    return find("input[name = '#{input_name}']").set "Valtech"
  end

  def execute_search(button_name)
    return click_button "#{button_name}"
  end

  def has_valtech(result_grid)
    return first("#{result_grid}").select_option
  end
end
