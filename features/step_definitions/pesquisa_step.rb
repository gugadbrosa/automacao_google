Dado("que acesso a seguinte página de busca:") do |table|
  site = table.rows_hash[:page_name]

  @search_page = SearchPage.new
  @search_page.open_search(site)
end

Quando("preencho o campo de busca com Valtech:") do |table|
  input_name = table.rows_hash[:search_input]
  button_name = table.rows_hash[:search_button]

  @search_page.search_field(input_name)

  @search_page.execute_search(button_name)
end

Então("a página retorna o site da empresa Valtech como primeiro resultado:") do |table|
  result_grid = table.rows_hash[:result_grid]

  expect(@search_page.has_valtech(result_grid)).to have_link ("https://www.valtech.com")
end
