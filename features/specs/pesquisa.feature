#language: pt


Funcionalidade: Pesquisa Valtech
    Sendo que sou um usuário que quer saber mais da Valtech
    Quero pesquisar sobre a empresa nos mecanismos de buscas
    Para que eu possa saber o site da empresa

    Esquema do Cenario: Pesquisar Valtech

        Dado que acesso a seguinte página de busca:
            | page_name | <page_name> |

        Quando preencho o campo de busca com Valtech:
            | search_input  | <search_input>  |
            | search_button | <search_button> |


        Então a página retorna o site da empresa Valtech como primeiro resultado:
            | result_grid | <result_grid> |


        Exemplos:
            | page_name               | search_input | search_button | result_grid    |
            | https://www.google.com/ | q            | btnK          | #rcnt          |
            | https://duckduckgo.com/ | q            | S             | #links_wrapper |
            | https://www.yahoo.com/  | p            | Buscar        | #web           |